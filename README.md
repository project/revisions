CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Provides a list of revisions for all the nodes available in your website.
Provides options to revert any node revision to its previous revision,
delete any node revision and filter nodes revision by user name and node
type from a single page.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/revisions

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/revisions


REQUIREMENTS
------------

This module doesn't require any module outside of Drupal core.


INSTALLATION
------------

 * Install this module as you would normally install a contributed
   Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * Go to /admin/config/nodes/revisions to see all the list of nodes revisions.


MAINTAINERS
-----------

 * Gaurav Kapoor (gaurav.kapoor) - https://www.drupal.org/u/gauravkapoor

Supporting organizations:

 * OpenSense Labs - https://www.drupal.org/opensense-labs
